#include "sheep.h"
#include <random.h>
#include <iostream>

using namespace std;

// standard deviations are a guess; sheep stats are hard to find
static Random rStanceWidth(Sheep::AVG_STANCE_WIDTH,5);
static Random rStanceLength(Sheep::AVG_STANCE_LENGTH,10);
static Random rFootSize(Sheep::AVG_FOOT_SIZE,0.1);
static Random rMass(Sheep::AVG_MASS,10);
static Random rStride(Sheep::AVG_STRIDE,5);

// assume a standard deviation of 30cm from the middle of the road
// this is a smaller variation than horses, since sheep tend to be in a tighter herd formation
static Random midpoint(0,30);

Sheep::Sheep() : Quadruped()
{
    mass = rMass.next() * 4; // synthetically increase the mass to increase the pressure exerted by their sharp, pointy feet
    stanceLength = rStanceLength.next();
    stanceWidth = rStanceWidth.next();
    footDiameter = rFootSize.next();
    strideLength = rStride.next();
}

void Sheep::traverse(Field &field)
{
    Quadruped::traverse(field, field.width()/2 + midpoint.next());
}
