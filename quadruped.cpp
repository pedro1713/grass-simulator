#include "quadruped.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <random.h>

using namespace cv;
using namespace std;

Quadruped::Quadruped()
{

}

Quadruped::~Quadruped()
{

}

void Quadruped::traverse(Field &field, double mid)
{
    double front = 0 - stanceLength * Random::drand();
    double back = front -stanceLength;

    double pressure = mass * 9.81 / (M_PI * pow(footDiameter / 2, 2));
    //cout << "Quadruped: " << pressure << "N/cm^2" << endl;

    while(back < field.length())
    {
        // positions of the four feet (front/back, left/right)
        Point2d fl(mid-stanceWidth/2, front);
        Point2d fr(mid+stanceWidth/2, front-strideLength/2);
        Point2d bl(mid-stanceWidth/2, back);
        Point2d br(mid+stanceWidth/2, back-strideLength/2);

        field.trample(fl,footDiameter, pressure);
        field.trample(fr,footDiameter, pressure);
        field.trample(bl,footDiameter, pressure);
        field.trample(br,footDiameter, pressure);

        front += strideLength;
        back += strideLength;
    }
}
