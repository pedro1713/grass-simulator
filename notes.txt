Variables:
	- horses/hour
	- carts/hour
	- people/hour
	- grass regrowth rate
	- grass death rate when trodden
	- foot size
	- cart wheel thickness
	- cart wheel spacing

Assumptions
	- circular feet
	- horses, carts centered using normal distribution
	- people tend to walk in pre-existing tracks
	- human gait:
		- length: 76.2cm (30 inches, established average)
		- width: 33.23cm (Clauser et al. -- chest breadth)
		- foot size: 24.78cm (Clauser et al.) x 
	- horse gait (https://www.horseloversmath.com/shoulder-angle-relate-stride-length/)
		- walking: 151.3cm (average from 7 horses, above)
		- width: 45.72cm (18 inches, https://www.fhwa.dot.gov/environment/recreational_trails/publications/fs_publications/07232816/page05.cfm)
		- foot size: 13cm
	- 
