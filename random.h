#ifndef RANDOM_H
#define RANDOM_H

#include <random>

/*!
 * \brief Wrapper for random number generator.  Produces numbers with a normal distribution
 */
class Random
{
public:
    Random(double mean, double deviation);
    double next();

    /*!
     * \brief Get a random number in the range 0-1 with an even distribution
     * \return
     */
    static double drand();

    /*!
     * \brief Generate an integer in the range 0-99 with an even distribution
     * \return
     */
    static int percent();

    /*!
     * \brief Roll an n-sided die, returning a result in the range 1-n (inclusive)
     * \param n The number of faces the die has
     * \return
     */
    static int dice(int n);

private:
    std::normal_distribution<double> distribution;
    std::default_random_engine generator;
};

#endif // RANDOM_H
