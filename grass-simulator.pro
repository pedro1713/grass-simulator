TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

OBJECTS_DIR = build/

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    field.cpp \
    thingy.cpp \
    cart.cpp \
    horse.cpp \
    person.cpp \
    random.cpp \
    sheep.cpp \
    quadruped.cpp

LIBS += -l:libopencv_core.so.3.0.0 \
        -l:libopencv_highgui.so.3.0.0 \
        -l:libopencv_imgproc.so.3.0.0 \
        -l:libopencv_video.so.3.0.0 \
        -l:libopencv_videoio.so.3.0.0 \
        -l:libopencv_imgcodecs.so.3.0.0

HEADERS += \
    field.h \
    thingy.h \
    cart.h \
    horse.h \
    person.h \
    random.h \
    sheep.h \
    quadruped.h
