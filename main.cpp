#include <iostream>
#include <field.h>
#include <horse.h>
#include <sheep.h>
#include <cart.h>
#include <person.h>
#include <random.h>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include <tclap/CmdLine.h>
#include <ctime>

using namespace std;
using namespace cv;

static std::string filename = "/dev/null";
static int fieldLength = 5;
static int fieldWidth = 3;
static int horsesPerDay = 0;
static int cartsPerDay = 0;
static int peoplePerDay = 0;
static int sheepPerDay = 0;
static int numDays = -1;
static double deathRate = 0.01;
static double safePressure = 25;
static bool showWindow = false;
static int fps = 30;

static int killRateSlider, pressureSlider;

static Field field(fieldLength,fieldWidth);

static int simulationDay = 0;

/*!
 * \brief Parse the command-line arguments, exiting with code 1 if there is an error
 * \param argc
 * \param argv
 */
static void parseArgs(int argc, const char **argv)
{
    using namespace TCLAP;
    try {
        CmdLine cmd("Grass Simulator: simulates what happens to a grassy field when humans, horses, and horse-drawn carts move through it",' ',"0.1",true);

        ValueArg<string> video("o","output","Name of the video file to save",false,filename,"file.avi");
        cmd.add(video);

        ValueArg<int> length("l","length","Length of the field in meters",false,fieldLength,"length");
        cmd.add(length);

        ValueArg<int> width("w","width","Width of the field in meters",false,fieldWidth,"width");
        cmd.add(width);

        ValueArg<int> horses("H","horses","Number of horses per day",false,horsesPerDay,"horses");
        cmd.add(horses);

        ValueArg<int> carts("C","carts","Number of carts per day",false,cartsPerDay,"carts");
        cmd.add(carts);

        ValueArg<int> people("P","people","Number of pedestrians per day",false,peoplePerDay,"people");
        cmd.add(people);

        ValueArg<int> sheep("S","sheep","Number of sheep per day",false,sheepPerDay,"sheep");
        cmd.add(sheep);

        ValueArg<int> days("t","time","Number of days to simulate",false,numDays,"days");
        cmd.add(days);

        ValueArg<int> start("d","start-day","Simulation day to start on",false,simulationDay,"day");
        cmd.add(start);

        ValueArg<double> death("k","kill","Amount of grass that is killed by trampling [0-1]",false,deathRate,"killed");
        cmd.add(death);

        ValueArg<int> framerate("f","fps","FPS of the output video",false,fps,"framerate");
        cmd.add(framerate);

        ValueArg<double> pressure("p","pressure","The pressure the ground can safely take before the kill rate increases (N/cm^2)",false,safePressure,"pressure");
        cmd.add(pressure);

        SwitchArg gui("W","window","Show a the state of the field at the end of every day",showWindow);
        cmd.add(gui);

        cmd.parse(argc,argv);

        filename = video.getValue();
        horsesPerDay = horses.getValue();
        cartsPerDay = carts.getValue();
        peoplePerDay = people.getValue();
        sheepPerDay = sheep.getValue();
        numDays = days.getValue();
        safePressure = pressure.getValue();
        deathRate = death.getValue();
        fieldLength = length.getValue();
        fieldWidth = width.getValue();
        showWindow = gui.getValue();
        fps = framerate.getValue();
        simulationDay = start.getValue();

    } catch(ArgException &e) {
        cerr << "Error parsing command-line parameters: " << e.error() << " for arg " << e.argId() << endl;
        exit(1);
    }
}

/*!
 * \brief Annotate the frame with some text indicating the simulation parameters
 * \param m The image to annotate
 * \param day The current day of the simulation
 */
static void annotate(Mat &m)
{
    stringstream ss;
    ss << "Day " << ((simulationDay+1) % 365) << " of year " << (simulationDay / 365 + 1) << " [" << field.season(simulationDay) << "]";
    Point p(12,12);
    putText(m,ss.str().c_str(),p,CV_FONT_HERSHEY_PLAIN,0.9,Scalar(0,0,0),1);

    ss.str("");
    ss << "Humans: " << peoplePerDay << " Horses: " << horsesPerDay << " Sheep: " << sheepPerDay << " Carts: " << cartsPerDay << " (/day)";
    p.y += 16;
    putText(m,ss.str().c_str(),p,CV_FONT_HERSHEY_PLAIN,0.9,Scalar(0,0,0),1);

    ss.str("");
    ss << (field.deathRateWhenTrampled * 100) << "% grass kill, " << (((int)(field.regrowthRate(simulationDay) * 10))/10.0) << "mm growth " << field.pressureResistence << "N/cm^2 safe load";
    p.y += 16;
    putText(m,ss.str().c_str(),p,CV_FONT_HERSHEY_PLAIN,0.9,Scalar(0,0,0),1);
}

/*!
 * \brief Convert the binary field image to a green RGB image
 * \param m
 */
static void colorize(Mat &m)
{
    Mat rb = Mat::zeros(m.rows,m.cols,CV_8UC1);
    vector<Mat> channels;
    channels.push_back(rb);
    channels.push_back(m);
    channels.push_back(rb);

    cv::merge(channels,m);
}

/*!
 * \brief Add a binary histogram showing the cross-section of the field
 * \param f
 * \param m
 */
static void addHistogram(Field &f, Mat &m)
{
    Mat hist = Mat::zeros(256,f.width(),m.type());

    for(int i=0; i<field.width(); i++)
    {
        UMat col = f.image.colRange(i,i+1);
        Scalar s = cv::mean(col);

        rectangle(hist,Point(i,255),Point(i,255-s[0]),Scalar(255,255,255),-1);
    }

    // shrink the graph in height
    const int histHeight = 45;
    resize(hist,hist,Size(hist.cols,histHeight));
    Rect roi(Point(0/*m.cols-hist.cols*/,m.rows-histHeight),Size(hist.cols,histHeight));
    Mat canvas = m(roi);
    hist.copyTo(canvas);
    putText(canvas,"Profile", Point(4,histHeight-8),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(0,0,0),1);
}

/*!
 * \brief Add a binary graph showing the grass growth curve for this season
 * \param f
 * \param m
 */
static void addGrowthCurve(Field &f, Mat &m)
{
    const int height = 45;
    Rect roi(Point(field.width(),m.rows-height),Size(m.cols - field.width(),height));
    Mat canvas = m(roi);

    int mid = canvas.cols/2;
    rectangle(canvas,Rect(Point(0,0),Size(canvas.cols,canvas.rows)),Scalar(0,0,0),-1);
    line(canvas,Point(mid,0),Point(mid,height),Scalar(255,255,255),1);
    for(int i=0; i<canvas.cols; i++)
    {
        int day = simulationDay-mid+i;
        double growth = f.regrowthRate(day);
        growth = (growth - Field::MIN_GROWTH)/(Field::MAX_GROWTH-Field::MIN_GROWTH);
        growth *= height;

        int h = height-(int)growth;
        rectangle(canvas,Rect(Point(i,h),Size(1,1)),Scalar(255,255,255),-1);

        if(i == mid)
        {
            line(canvas,Point(i-5,h),Point(i+5,h),Scalar(0,0,0),1);
        }
    }
    putText(canvas,"Growth Rate", Point(4,height-8),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,255),1);
}

/*!
 * \brief Save an image to a PNG file named for the current date and time (e.g. 20160227150358.png)
 * \param m
 */
void save(Mat m)
{
    char date[64];
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(date,64,"%Y%m%d%I%M%S",timeinfo);

    stringstream ss;
    ss << date << "_P" << peoplePerDay << "_H" << horsesPerDay << "_C" << cartsPerDay << "_S" << sheepPerDay << ".png";

    std::string filename =  ss.str();

    cout << "Saving image as " << filename << endl;

    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(80);
    imwrite(filename,m,params);
}

/*!
 * \brief Callback function for the sliders
 * \param value
 * \param arg
 */
static void trackbarCallback(int value, void *arg)
{
    (void)value;

    if(arg == &killRateSlider)
        field.deathRateWhenTrampled = (double)killRateSlider / 100.0;
    else if(arg == &pressureSlider)
        field.pressureResistence = (double)pressureSlider;
}

/*!
 * \brief Initialize the GUI
 */
static void initGui()
{
    namedWindow("Field");
    moveWindow("Field",0,0);

    killRateSlider = (int)(deathRate * 100);
    pressureSlider = (int)(safePressure);
    cv::createTrackbar("Humans/day","Field",&peoplePerDay,100);
    cv::createTrackbar("Horses/day","Field",&horsesPerDay,100);
    cv::createTrackbar("Carts/day","Field",&cartsPerDay,100);
    cv::createTrackbar("Kill %","Field",&killRateSlider,100,trackbarCallback,&killRateSlider);
    cv::createTrackbar("Pressure Lim.","Field", &pressureSlider, 5000,trackbarCallback,&pressureSlider);
}

/*!
 * \brief Run the simulation
 * \param argc
 * \param argv
 * \return
 */
int main(int argc, const char **argv)
{
    vector<Thingy*> thingies;

    parseArgs(argc,argv);

    field.deathRateWhenTrampled = deathRate;
    field.pressureResistence = safePressure;

    if(filename == "/dev/null" && !showWindow)
    {
        cout << "Not saving video and not showing GUI." << endl <<
                "This is useless. All we're doing is wasting CPU cycles" << endl <<
                "Please re-run with either the -W or -o options" << endl;
        return 0;
    }

    cout << "Writing video to " << filename << endl;
    VideoWriter video;
    video = VideoWriter(filename,CV_FOURCC('D','I','V','X'),fps,Size(field.image.rows,field.image.cols),true);

    Mat frame;
    char spinbox[] = {'|','/','-','\\'};

    if(showWindow)
        initGui();
    else
        cout << spinbox[0];

    char ch = 0;

    for(int d=0; (d<numDays || numDays < 0) && ch != 'q'; d++)
    {

        while(!thingies.empty())
        {
            delete thingies.back();
            thingies.pop_back();
        }

        for(int i=0; i<horsesPerDay; i++)
            thingies.push_back(new Horse());
        for(int i=0; i<sheepPerDay; i++)
            thingies.push_back(new Sheep());
        for(int i=0; i<peoplePerDay; i++)
            thingies.push_back(new Person());
        for(int i=0; i<cartsPerDay; i++)
        {
            // 25% of carts have 2 axles
            // of those, 35% have 1 horse, 60% have 2 horses, and the remainder have 4 horses
            int numAxles;
            int numHorses;

            if(rand() % 4)
            {
                numHorses = 1;
                numAxles = 1;
            }
            else
            {
                numAxles = 2;
                int percent = Random::percent();
                if(percent < 35)
                    numHorses = 1;
                else if(percent < 95)
                    numHorses = 2;
                else
                    numHorses = 4;
            }

            thingies.push_back(new Cart(numAxles,numHorses));
        }

        for_each(thingies.begin(), thingies.end(), [&](Thingy *t){
           t->traverse(field);
        });

        //transpose(field.image,frame);
        //video.write(frame);
        field.regrow(simulationDay);
        transpose(field.image,frame);
        colorize(frame);
        annotate(frame);
        addHistogram(field,frame);
        addGrowthCurve(field,frame);

        video.write(frame);

        if(showWindow)
        {
            imshow("Field",frame);
            ch = cvWaitKey(1);
            if(ch == 'q')
                break;
            else if(ch == 's')
                save(frame);

        }
        else
        {
            cout << '\r' << spinbox[d % 4] << "\tDay " << (d+1) << " of " << numDays;
            cout.flush();
        }

        simulationDay++;
    }
    video.release();

    if(!showWindow)
        cout << "\r                                         " << endl;

    cout << "Simulation terminated" << endl;
    if(ch != 'q' && showWindow)
    {
        cout << "Press any key to exit" << endl;
        ch = cvWaitKey(0);
        if(ch == 's')
        {
            save(frame);
        }
    }

    return 0;
}

