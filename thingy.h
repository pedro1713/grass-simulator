#ifndef THINGY_H
#define THINGY_H

#include <field.h>

/*!
 * \brief A thing that can move across the field
 */
class Thingy
{
public:
    Thingy();
    virtual ~Thingy();

    /*!
     * \brief Traverse the field, trampling the grass that the thingy steps on/rolls over
     * \param field
     */
    virtual void traverse(Field &field) = 0;

protected:
    double mass;
};

#endif // THINGY_H
